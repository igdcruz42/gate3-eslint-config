module.exports = {

	rules: {
		semi: ["error", "always"],
		indent: ["warn", "tab"]
	}

};